<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="OAuth2" FOLDED="false" ID="ID_1743405846" CREATED="1522482882548" MODIFIED="1522482893990" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Auth Flow" POSITION="right" ID="ID_256657880" CREATED="1522482894243" MODIFIED="1522482904915">
<node TEXT="Implicit Grant" ID="ID_781996469" CREATED="1522482908543" MODIFIED="1522482935414">
<node TEXT="Hybrid mobile App or Client-side WebApp" ID="ID_1277172152" CREATED="1522482973459" MODIFIED="1522482998220">
<node TEXT="Possui menos confian&#xe7;a por n&#xe3;o ter servidor confi&#xe1;vel no meio" ID="ID_592487852" CREATED="1522483017278" MODIFIED="1522483049206">
<node TEXT="Acarreta no n&#xe3;o uso de refresh_token para retomar conex&#xe3;o" ID="ID_1611199902" CREATED="1522483050413" MODIFIED="1522483075705"/>
</node>
</node>
</node>
<node TEXT="Authorization Code using Proof Key for Code Exchange (PKCE)" ID="ID_1619705012" CREATED="1522482947379" MODIFIED="1522482952596">
<node TEXT="Native mobile App" ID="ID_893946532" CREATED="1522482963858" MODIFIED="1522482969315">
<node TEXT="App troca id&#xe9;ia com servidor de autentica&#xe7;&#xe3;o para provar que &#xe9; leg&#xed;timo" ID="ID_1875102196" CREATED="1522483086634" MODIFIED="1522483128561">
<node TEXT="Servidor de autentica&#xe7;&#xe3;o entrega Access token, que permite uso da API" ID="ID_579320509" CREATED="1522483130122" MODIFIED="1522483155755"/>
</node>
</node>
</node>
<node TEXT="Authorization Code" ID="ID_672886370" CREATED="1522482953541" MODIFIED="1522482959519">
<node TEXT="Server-side WebApp" ID="ID_556098179" CREATED="1522483002192" MODIFIED="1522483010801">
<node TEXT="Usu&#xe1;rio concorda na interface web com o compartilhamento de informa&#xe7;&#xf5;es" ID="ID_1346989529" CREATED="1522483163933" MODIFIED="1522483184844">
<node TEXT="Servidor de autentica&#xe7;&#xe3;o entrega ao servidor Access token para usar com a API" ID="ID_433893460" CREATED="1522483186190" MODIFIED="1522483203550"/>
</node>
</node>
</node>
</node>
</node>
</map>
